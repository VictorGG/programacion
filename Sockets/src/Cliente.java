import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Cliente{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Conectando al servidor...");
            Socket cliente = new Socket("localhost", 50005);
            System.out.println("Conexión establecida.");
            DataInputStream entrada = new DataInputStream(cliente.getInputStream());
            DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());
            String mensaje;

            while (true) {
                mensaje = entrada.readUTF();
                System.out.println("Servidor: " + mensaje);

                if (mensaje.equalsIgnoreCase("salir")) {
                    System.out.println("El servidor ha cerrado la conexión.");
                    break;
                }

                String respuesta = scanner.nextLine();
                salida.writeUTF(respuesta);

                if (respuesta.equalsIgnoreCase("salir")) {
                    System.out.println("Solicitando cierre de la conexión...");
                    break;
                }
            }

            salida.close();
            entrada.close();
            cliente.close();
            System.out.println("Conexión cerrada.");

        } catch (IOException e) {
            System.err.println("Error al conectar con el servidor: " + e.getMessage());
        }
    }
}