import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor{
    public static void main(String[] args) {
        try {
            ServerSocket serversocket = new ServerSocket(50005);
            System.out.println("Servidor iniciado. Esperando conexiones...");

            while (true) {
                Socket cliente = serversocket.accept();
                System.out.println("Cliente conectado.");
                DataInputStream entrada = new DataInputStream(cliente.getInputStream());
                DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());

                while (true) {
                    salida.writeUTF("Dime el primer numero o \"salir\": ");
                    String primero = entrada.readUTF();

                    if (primero.equalsIgnoreCase("salir")) {
                        salida.writeUTF("salir");
                        break;
                    }

                    salida.writeUTF("Dime el segundo numero: ");
                    String segundo = entrada.readUTF();

                    salida.writeUTF("Dime el operador [+,-,*,/]: ");
                    String operador = entrada.readUTF();

                    double resultado = calcular(Double.parseDouble(primero), Double.parseDouble(segundo), operador.charAt(0));
                    salida.writeUTF(String.valueOf(resultado));
                }

                System.out.println("Cliente desconectado.");
                cliente.close();
            }
        } catch (IOException e) {
            System.err.println("Error en el servidor: " + e.getMessage());
        }
    }

    private static double calcular(double n1, double n2, char operador) {
        switch (operador) {
            case '+':
                return n1 + n2;
            case '-':
                return n1 - n2;
            case '*':
                return n1 * n2;
            case '/':
                if (n2 != 0) {
                    return n1 / n2;
                } else {
                    return Double.NaN;
                }
            default:
                return Double.NaN;
        }
    }
}
