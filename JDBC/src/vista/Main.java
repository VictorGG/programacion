package vista;

import controlador.JugadoresJDBC;
import utilidades.Conexion;
import modelo.Jugadores;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Conexion c = new Conexion();
        Connection conn = null;
        PreparedStatement stmt = null;
        try {

            System.out.println("Connecting to database...");
            conn = c.getConnection();
            JugadoresJDBC controller = new JugadoresJDBC(conn);

            ArrayList<Jugadores> lista = controller.selectall();
            for (Jugadores u : lista) {
                System.out.println(u);
            }


        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
    }
}
