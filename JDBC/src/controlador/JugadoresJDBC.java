package controlador;

import modelo.Jugadores;
import modelo.JugadoresDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.*;

public class JugadoresJDBC implements JugadoresDAO {
    public final String SELECT_ALL = "SELECT * FROM jugadores";
    public final String SELECT_BY_ID = "SELECT * FROM jugadores WHERE id = ?";
    public final String SELECT_MAX_EDAD = "SELECT MAX(edad) FROM equipos";
    public final String INSERT_JUGADORES = "INSERT jugadores VALUES(?, ?, ?, ?)";
    public final String DELETE_JUGADOR_BY_ID = "DELETE FROM jugadores WHERE id = ?";
    public final String UPDATE_EQUIPO_JUGADOR_BY_ID_JUGADOR = "UPDATE jugadores SET id_equipo = ? WHERE id = ?";
    public final String UPDATE_ALL_BY_ID_JUGADOR = "UPDATE jugadores SET nombre_jugador = ?, edad = ?, nacionalidad = ?, posicion = ?, id_equipo = ? WHERE id = ?";
    Connection con;
    public JugadoresJDBC(Connection con){
        this.con= con;
    }

    @Override
    public ArrayList<Jugadores> selectall() throws SQLException {
        ArrayList<Jugadores> lista_jugadores = new ArrayList<Jugadores>();
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                Date fecha_nacimiento = rs.getDate("fecha_nacimiento");
                float altura = rs.getFloat("altura");
                String posicion = rs.getString("posicion");
                int equipo_id = rs.getInt("equipo_id");
                Jugadores j1 = new Jugadores(id, nombre, fecha_nacimiento, altura, posicion, equipo_id);
                lista_jugadores.add(j1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_jugadores;
    }

    @Override
    public Jugadores select_by_id(int id) {
        Jugadores j1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int id2 = rs.getInt("id");
                String nombre = rs.getString("nombre");
                Date fecha_nacimiento = rs.getDate("fecha_nacimiento");
                float altura = rs.getFloat("altura");
                String posicion = rs.getString("posicion");
                int equipo_id = rs.getInt("equipo_id");

                j1 = new Jugadores(id2, nombre,fecha_nacimiento,altura,posicion,equipo_id);

            }else{
                j1 = null;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return j1;
    }

    @Override
    public double select_max_edad() {
        double max_edad = 0;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_MAX_EDAD);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                max_edad = rs.getDouble("max_edad");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return max_edad;
    }

    @Override
    public boolean insert_jugador(Jugadores jugador) {
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_JUGADORES);
            stmt.setInt(1, jugador.getId());
            stmt.setString(2, jugador.getNombre());
            stmt.setDate(3, (Date) jugador.getFecha_nacimiento());
            stmt.setFloat(4, jugador.getAltura());
            stmt.setString(5, jugador.getPosicion());
            stmt.setInt(6, jugador.getEquipo_id());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    @Override
    public boolean delete_jugador_by_id(int id) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_JUGADOR_BY_ID);
            stmt.setInt(1, id);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_equipo_jugador_by_id_jugador(int id, int id_equipo) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_EQUIPO_JUGADOR_BY_ID_JUGADOR);
            stmt.setInt(1, id_equipo);
            stmt.setInt(2, id);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }
    @Override
    public boolean update_all_by_id_jugador(int id, String nombre, Date fecha_nacimiento, float altura, String posicion, int equipo_id){
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_ALL_BY_ID_JUGADOR);
            stmt.setInt(1, id);
            stmt.setString(2, nombre);
            stmt.setDate(3, fecha_nacimiento);
            stmt.setFloat(4, altura);
            stmt.setString(5, posicion);
            stmt.setInt(6, equipo_id);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

}
