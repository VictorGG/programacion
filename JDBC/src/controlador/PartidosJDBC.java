package controlador;

import modelo.Equipos;
import modelo.Partidos;
import modelo.PartidosDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class PartidosJDBC implements PartidosDAO {
    public final String SELECT_ALL = "SELECT * FROM partidos";
    public final String SELECT_BY_ID = "SELECT * FROM partidos WHERE id = ?";
    public final String SELECT_MAX_PUNTOS_EQUIPO_LOCAL = "SELECT MAX(puntos_equipo_local) FROM partidos";
    public final String INSERT_PARTIDO = "INSERT partidos VALUES(?, ?, ?, ?, ?, ?)";
    public final String DELETE_PARTIDO_BY_ID = "DELETE FROM partidos WHERE id = ?";
    public final String UPDATE_FECHA_BY_ID = "UPDATE partidos SET fecha = ? WHERE id = ?";
    public final String UPDATE_ALL_BY_ID = "UPDATE partido SET fecha = ?, equipo_local_id = ?, equipo_visitante_id = ?, puntos_equipo_local = ?, puntos_equipo_visitante = ? WHERE id = ?";
    Connection con;
    public PartidosJDBC(Connection con){
        this.con= con;
    }


    @Override
    public ArrayList<Partidos> selectall() throws SQLException {
        ArrayList<Partidos> lista_partidos = new ArrayList<>();
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                Date fecha = rs.getDate("fecha");
                int equipo_local_id = rs.getInt("equipo_local_id");
                int equipo_visitante_id = rs.getInt("equipo_visitante_id");
                int puntos_equipo_local = rs.getInt("puntos_equipo_local");
                int puntos_equipo_visitante = rs.getInt("puntos_equipo_visitante");

                Partidos p1 = new Partidos(id,fecha,equipo_local_id,equipo_visitante_id,puntos_equipo_local,puntos_equipo_visitante);
                lista_partidos.add(p1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_partidos;
    }

    @Override
    public Partidos select_by_id(int id) {
        Partidos p1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int id2 = rs.getInt("id");
                Date fecha = rs.getDate("fecha");
                int equipo_local_id = rs.getInt("equipo_local_id");
                int equipo_visitante_id = rs.getInt("equipo_visitante_id");
                int puntos_equipo_local = rs.getInt("puntos_equipo_local");
                int puntos_equipo_visitante = rs.getInt("puntos_equipo_visitante");

                p1 = new Partidos(id2,fecha,equipo_local_id,equipo_visitante_id,puntos_equipo_local,puntos_equipo_visitante);

            }else{
                p1 = null;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return p1;
    }

    @Override
    public int select_max_puntos_equipo_local() {
        int max_puntos_equipo_local = 0;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_MAX_PUNTOS_EQUIPO_LOCAL);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                max_puntos_equipo_local = rs.getInt("max_puntos_equipo_local");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return max_puntos_equipo_local;
    }

    @Override
    public boolean insert_partido(Partidos partidos) {
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_PARTIDO);
            stmt.setInt(1, partidos.getId());
            stmt.setDate(2, (java.sql.Date) partidos.getFecha());
            stmt.setInt(3, partidos.getEquipo_local_id());
            stmt.setInt(4, partidos.getEquipo_visitante_id());
            stmt.setInt(5, partidos.getPuntos_equipo_local());
            stmt.setInt(6, partidos.getPuntos_equipo_visitante());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    @Override
    public boolean delete_partido_by_id(int id) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_PARTIDO_BY_ID);
            stmt.setInt(1, id);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_fecha_by_id(int id, Date fecha) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_FECHA_BY_ID);
            stmt.setDate(1, (java.sql.Date) fecha);
            stmt.setInt(2, id);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_all_by_id(int id, Date fecha, int equipo_local_id, int equipo_visitante_id, int puntos_equipo_local, int puntos_equipo_visitante) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_ALL_BY_ID);
            stmt.setInt(1, id);
            stmt.setDate(2, (java.sql.Date) fecha);
            stmt.setInt(3, equipo_local_id);
            stmt.setInt(4, equipo_visitante_id);
            stmt.setInt(5, puntos_equipo_local);
            stmt.setInt(6, puntos_equipo_visitante);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }
}
