package controlador;

import modelo.Equipos;
import modelo.EquiposDAO;
import modelo.Jugadores;

import java.sql.*;
import java.util.ArrayList;

public class EquiposJDBC implements EquiposDAO {
    public final String SELECT_ALL = "SELECT * FROM equipos";
    public final String SELECT_BY_ID = "SELECT * FROM equipos WHERE id = ?";
    public final String SELECT_MAX_CAMPEONATOS = "SELECT MAX(campeonatos_ganados) FROM equipos";
    public final String INSERT_EQUIPO = "INSERT equipos VALUES(?, ?, ?, ?, ?, ?, ?)";
    public final String DELETE_EQUIPO_BY_ID = "DELETE FROM equipos WHERE id = ?";
    public final String UPDATE_CAMPEONATOS_BY_ID = "UPDATE equipos SET campeonatos_ganados = ? WHERE id = ?";
    public final String UPDATE_ALL_BY_ID = "UPDATE equipos SET nombre = ?, ciudad = ?, fundacion = ?, estadio = ?, campeonatos_ganados = ?, entrenador_principal = ? WHERE id = ?";
    Connection con;
    public EquiposJDBC(Connection con){
        this.con= con;
    }


    @Override
    public ArrayList<Equipos> selectall() throws SQLException {
        ArrayList<Equipos> lista_equipos = new ArrayList<Equipos>();
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String ciudad = rs.getString("ciudad");
                String fundacion = rs.getString("fundacion");
                String estadio = rs.getString("estadio");
                int campeonatos_ganados = rs.getInt("campeonatos_ganados");
                String entrenador_principal = rs.getString("entrenador_principal");

                Equipos e1 = new Equipos(id,nombre,ciudad,fundacion,estadio,campeonatos_ganados,entrenador_principal);
                lista_equipos.add(e1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_equipos;
    }

    @Override
    public Equipos select_by_id(int id) {
        Equipos e1 = null;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int id2 = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String ciudad = rs.getString("ciudad");
                String fundacion = rs.getString("fundacion");
                String estadio = rs.getString("estadio");
                int campeonatos_ganados = rs.getInt("campeonatos_ganados");
                String entrenador_principal = rs.getString("entrenador_principal");

                e1 = new Equipos(id2,nombre,ciudad,fundacion,estadio,campeonatos_ganados,entrenador_principal);

            }else{
                e1 = null;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return e1;
    }

    @Override
    public int select_max_campeonatos_ganados() {
        int max_campeonatos_ganados = 0;
        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_MAX_CAMPEONATOS);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                max_campeonatos_ganados = rs.getInt("max_campeonatos_ganados");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return max_campeonatos_ganados;
    }

    @Override
    public boolean insert_equipo(Equipos equipos) {
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_EQUIPO);
            stmt.setInt(1, equipos.getId());
            stmt.setString(2, equipos.getNombre());
            stmt.setString(3, equipos.getCiudad());
            stmt.setString(4, equipos.getFundacion());
            stmt.setString(5, equipos.getEstadio());
            stmt.setInt(6, equipos.getCampeonatos_ganados());
            stmt.setString(7, equipos.getEntrenador_principal());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    @Override
    public boolean delete_equipo_by_id(int id) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_EQUIPO_BY_ID);
            stmt.setInt(1, id);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_campeonatos_by_id(int id, int campeonatos_ganados) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_CAMPEONATOS_BY_ID);
            stmt.setInt(1, campeonatos_ganados);
            stmt.setInt(2, id);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }

    @Override
    public boolean update_all_by_id(int id, String nombre, String ciudad, String fundacion, String estadio, int campeonatos_ganados, String entrenador_principal) {
        boolean salida = true;
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_ALL_BY_ID);
            stmt.setInt(1, id);
            stmt.setString(2, nombre);
            stmt.setString(3, ciudad);
            stmt.setString(4, fundacion);
            stmt.setString(5, estadio);
            stmt.setInt(6, campeonatos_ganados);
            stmt.setString(7, entrenador_principal);

            int x = stmt.executeUpdate();

            if(x!=1){
                salida = false;
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return salida;
    }
}
