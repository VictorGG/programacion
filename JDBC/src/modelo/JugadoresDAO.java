package modelo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public interface JugadoresDAO {
    ArrayList<Jugadores> selectall() throws SQLException;

    Jugadores select_by_id(int id_jugador);

    double select_max_edad();

    boolean insert_jugador(Jugadores jugador);

    boolean delete_jugador_by_id(int id_jugador);

    boolean update_equipo_jugador_by_id_jugador(int id, int id_equipo);

    boolean update_all_by_id_jugador(int id, String nombre, java.sql.Date fecha_nacimiento, float altura, String posicion, int equipo_id);
}