package modelo;

import java.util.Date;

public class Jugadores {
    private int id;
    private String nombre;
    private Date fecha_nacimiento;
    private float altura;
    private String posicion;
    private int equipo_id;

    public Jugadores(int id, String nombre, Date fecha_nacimiento, float altura, String posicion, int equipo_id) {
        this.id = id;
        this.nombre = nombre;
        this.fecha_nacimiento = fecha_nacimiento;
        this.altura = altura;
        this.posicion = posicion;
        this.equipo_id = equipo_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public int getEquipo_id() {
        return equipo_id;
    }

    public void setEquipo_id(int equipo_id) {
        this.equipo_id = equipo_id;
    }

    @Override
    public String toString() {
        return "Jugadores{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", fecha_nacimiento=" + fecha_nacimiento +
                ", altura=" + altura +
                ", posicion='" + posicion + '\'' +
                ", equipo_id=" + equipo_id +
                '}';
    }
}