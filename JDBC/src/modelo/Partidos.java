package modelo;

import java.util.Date;

public class Partidos {
    private int id;
    private Date fecha;
    private int equipo_local_id;
    private int equipo_visitante_id;
    private int puntos_equipo_local;
    private int puntos_equipo_visitante;

    public Partidos(int id, Date fecha, int equipo_local_id, int equipo_visitante_id, int puntos_equipo_local, int puntos_equipo_visitante) {
        this.id = id;
        this.fecha = fecha;
        this.equipo_local_id = equipo_local_id;
        this.equipo_visitante_id = equipo_visitante_id;
        this.puntos_equipo_local = puntos_equipo_local;
        this.puntos_equipo_visitante = puntos_equipo_visitante;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getEquipo_local_id() {
        return equipo_local_id;
    }

    public void setEquipo_local_id(int equipo_local_id) {
        this.equipo_local_id = equipo_local_id;
    }

    public int getEquipo_visitante_id() {
        return equipo_visitante_id;
    }

    public void setEquipo_visitante_id(int equipo_visitante_id) {
        this.equipo_visitante_id = equipo_visitante_id;
    }

    public int getPuntos_equipo_local() {
        return puntos_equipo_local;
    }

    public void setPuntos_equipo_local(int puntos_equipo_local) {
        this.puntos_equipo_local = puntos_equipo_local;
    }

    public int getPuntos_equipo_visitante() {
        return puntos_equipo_visitante;
    }

    public void setPuntos_equipo_visitante(int puntos_equipo_visitante) {
        this.puntos_equipo_visitante = puntos_equipo_visitante;
    }

    @Override
    public String toString() {
        return "Partidos{" +
                "id=" + id +
                ", fecha=" + fecha +
                ", equipo_local_id=" + equipo_local_id +
                ", equipo_visitante_id=" + equipo_visitante_id +
                ", puntos_equipo_local=" + puntos_equipo_local +
                ", puntos_equipo_visitante=" + puntos_equipo_visitante +
                '}';
    }
}
