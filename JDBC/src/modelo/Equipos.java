package modelo;

public class Equipos {
    private int id;
    private String nombre;
    private String ciudad;
    private String fundacion;
    private String estadio;
    private int campeonatos_ganados;
    private String entrenador_principal;

    public Equipos(int id, String nombre, String ciudad, String fundacion, String estadio, int campeonatos_ganados, String entrenador_principal) {
        this.id = id;
        this.nombre = nombre;
        this.ciudad = ciudad;
        this.fundacion = fundacion;
        this.estadio = estadio;
        this.campeonatos_ganados = campeonatos_ganados;
        this.entrenador_principal = entrenador_principal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getFundacion() {
        return fundacion;
    }

    public void setFundacion(String fundacion) {
        this.fundacion = fundacion;
    }

    public String getEstadio() {
        return estadio;
    }

    public void setEstadio(String estadio) {
        this.estadio = estadio;
    }

    public int getCampeonatos_ganados() {
        return campeonatos_ganados;
    }

    public void setCampeonatos_ganados(int campeonatos_ganados) {
        this.campeonatos_ganados = campeonatos_ganados;
    }

    public String getEntrenador_principal() {
        return entrenador_principal;
    }

    public void setEntrenador_principal(String entrenador_principal) {
        this.entrenador_principal = entrenador_principal;
    }

    @Override
    public String toString() {
        return "Equipos{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", ciudad='" + ciudad + '\'' +
                ", fundacion='" + fundacion + '\'' +
                ", estadio='" + estadio + '\'' +
                ", campeonatos_ganados=" + campeonatos_ganados +
                ", entrenador_principal='" + entrenador_principal + '\'' +
                '}';
    }
}