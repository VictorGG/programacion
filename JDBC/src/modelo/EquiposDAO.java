package modelo;

import java.sql.SQLException;
import java.util.ArrayList;

public interface EquiposDAO {
    ArrayList<Equipos> selectall() throws SQLException;

    Equipos select_by_id(int id);

    int select_max_campeonatos_ganados();

    boolean insert_equipo(Equipos equipos);

    boolean delete_equipo_by_id(int id);

    boolean update_campeonatos_by_id(int id, int campeonatos_ganados);

    boolean update_all_by_id(int id, String nombre, String ciudad, String fundacion, String estadio, int campeonatos_ganados, String entrenador_principal);
}
