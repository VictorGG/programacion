package modelo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public interface PartidosDAO {
    ArrayList<Partidos> selectall() throws SQLException;

    Partidos select_by_id(int id);

    int select_max_puntos_equipo_local();

    boolean insert_partido(Partidos partidos);

    boolean delete_partido_by_id(int id);

    boolean update_fecha_by_id(int id, Date fecha);

    boolean update_all_by_id(int id, Date fecha, int equipo_local_id, int equipo_visitante_id, int puntos_equipo_local,int puntos_equipo_visitante);
}
