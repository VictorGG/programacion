CREATE DATABASE IF NOT EXISTS basketball_db;
USE basketball_db;

CREATE TABLE IF NOT EXISTS equipos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    ciudad VARCHAR(100),
    fundacion DATE,
    estadio VARCHAR(100),
    campeonatos_ganados INT,
    entrenador_principal VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS jugadores (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    fecha_nacimiento DATE,
    altura FLOAT,
    posicion VARCHAR(50),
    equipo_id INT,
    FOREIGN KEY (equipo_id) REFERENCES equipos(id)
);

CREATE TABLE IF NOT EXISTS partidos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    fecha DATE,
    equipo_local_id INT,
    equipo_visitante_id INT,
    puntos_equipo_local INT,
    puntos_equipo_visitante INT,
    FOREIGN KEY (equipo_local_id) REFERENCES equipos(id),
    FOREIGN KEY (equipo_visitante_id) REFERENCES equipos(id)
);

INSERT INTO equipos (nombre, ciudad, fundacion, estadio, campeonatos_ganados, entrenador_principal) 
VALUES 
('Los Angeles Lakers', 'Los Angeles', '1947-01-01', 'Staples Center', 17, 'Frank Vogel'),
('Golden State Warriors', 'San Francisco', '1946-01-01', 'Chase Center', 6, 'Steve Kerr'),
('Chicago Bulls', 'Chicago', '1966-01-01', 'United Center', 6, 'Billy Donovan'),
('Boston Celtics', 'Boston', '1946-01-01', 'TD Garden', 17, 'Ime Udoka'),
('Miami Heat', 'Miami', '1988-01-01', 'American Airlines Arena', 3, 'Erik Spoelstra');

INSERT INTO jugadores (nombre, fecha_nacimiento, altura, posicion, equipo_id) 
VALUES 
('LeBron James', '1984-12-30', 2.06, 'Alero', 1),
('Stephen Curry', '1988-03-14', 1.91, 'Base', 2),
('Michael Jordan', '1963-02-17', 1.98, 'Escolta', 3),
('Larry Bird', '1956-12-07', 2.06, 'Alero', 4),
('Dwyane Wade', '1982-01-17', 1.93, 'Escolta', 5);

INSERT INTO partidos (fecha, equipo_local_id, equipo_visitante_id, puntos_equipo_local, puntos_equipo_visitante) 
VALUES 
('2024-03-15', 1, 2, 110, 105),
('2024-03-16', 3, 4, 95, 98),
('2024-03-17', 5, 1, 100, 98),
('2024-03-18', 2, 3, 112, 105),
('2024-03-19', 4, 5, 102, 97);
