import java.util.concurrent.Semaphore;

class ATMMain {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3); // semáforo con 3 permisos (cajeros disponibles)
        for (int i = 1; i <= 5; i++) {
            ATM atm = new ATM("ATM" + i, semaphore);
            atm.start();
        }
    }
}
