import java.util.Random;
import java.util.concurrent.Semaphore;

class ATM extends Thread {
    String name;
    Random random = new Random();
    Semaphore semaphore;

    public ATM(String name, Semaphore semaphore) {
        this.name = name;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            semaphore.acquire(); // adquirir el permiso para utilizar el cajero
            int tempo = random.nextInt(8)+3; // tiempo aleatorio entre 3 y 10 segundos
            for (int i = 0; i < tempo; i++) {
                System.out.println("ATM " + name + " está en uso");
                Thread.sleep(1000);
            }
            System.out.println("ATM " + name + " ha sido liberado");
            semaphore.release(); // liberar el cajero una vez que se haya terminado de utilizar
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}