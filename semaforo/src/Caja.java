import java.util.Random;
import java.util.concurrent.Semaphore;

class Caja implements Runnable {
    String nombre;
    Random random = new Random();
    Semaphore semaphore;

    public Caja(String nombre, Semaphore semaphore) {
        this.nombre = nombre;
        this.semaphore = semaphore;
    }

    public void procesarCompra(String cliente, int productos) {
        System.out.println("La " + nombre + " recibe al cliente " + cliente);
        for (int i = 1; i <= productos; i++) {
            try {
                Thread.sleep(random.nextInt(11000)); // tiempo aleatorio entre 0 y 10 segundos para procesar cada producto
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Caja " + nombre + " - Cliente: " + cliente + " - Producto " + i);
        }
    }

    @Override
    public void run() {
        try {
            semaphore.acquire(); // adquirir el permiso para utilizar la caja
            procesarCompra("Cliente", random.nextInt(6) + 1); // número aleatorio de productos entre 1 y 6
            semaphore.release(); // liberar la caja una vez que se haya terminado de utilizar
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
