import java.util.concurrent.Semaphore;

class Supermercado {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(2);

        Caja caja_1 = new Caja("Caja 1", semaphore);
        Caja caja_2 = new Caja("Caja 2", semaphore);
        Caja caja_3 = new Caja("Caja 3", semaphore);

        CajaHilo hiloCaja1 = new CajaHilo(caja_1);
        CajaHilo hiloCaja2 = new CajaHilo(caja_2);
        CajaHilo hiloCaja3 = new CajaHilo(caja_3);

        hiloCaja1.start();
        hiloCaja2.start();
        hiloCaja3.start();
    }
}