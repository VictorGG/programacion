class CajaHilo extends Thread {
    Caja caja;

    public CajaHilo(Caja caja) {
        this.caja = caja;
    }

    @Override
    public void run() {
        caja.run();
    }
}
