package com.example.portfolio.service;

import com.example.portfolio.model.Header;
import com.example.portfolio.model.Sobre_mi;
import com.example.portfolio.repository.HeaderRepository;
import com.example.portfolio.repository.Sobre_miRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Sobre_miService {
    @Autowired
    private Sobre_miRepository sobre_miRepository;

    public List<Sobre_mi> getAll() {
        return sobre_miRepository.findAll();
    }
}