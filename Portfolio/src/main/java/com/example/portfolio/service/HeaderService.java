package com.example.portfolio.service;

import com.example.portfolio.model.Header;
import com.example.portfolio.repository.HeaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HeaderService {
    @Autowired
    private HeaderRepository headerRepository;

    public List<Header> getAll() {
        return headerRepository.findAll();
    }
}