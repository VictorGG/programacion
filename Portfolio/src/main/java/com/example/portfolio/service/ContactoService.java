package com.example.portfolio.service;

import com.example.portfolio.model.Contacto;
import com.example.portfolio.model.Proyectos;
import com.example.portfolio.repository.ContactoRepository;
import com.example.portfolio.repository.ProyectosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactoService {
    @Autowired
    private ContactoRepository contactoRepository;

    public List<Contacto> getAll() {
        return contactoRepository.findAll();
    }

    public void addContacto(Contacto c) {

    }
}