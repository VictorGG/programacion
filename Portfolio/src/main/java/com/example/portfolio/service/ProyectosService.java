package com.example.portfolio.service;

import com.example.portfolio.model.Proyectos;
import com.example.portfolio.model.Sobre_mi;
import com.example.portfolio.repository.ProyectosRepository;
import com.example.portfolio.repository.Sobre_miRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProyectosService {
    @Autowired
    private ProyectosRepository proyectosRepository;

    public List<Proyectos> getAll() {
        return proyectosRepository.findAll();
    }
}