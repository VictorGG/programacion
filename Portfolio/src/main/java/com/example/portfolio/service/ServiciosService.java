package com.example.portfolio.service;

import com.example.portfolio.model.Proyectos;
import com.example.portfolio.model.Servicios;
import com.example.portfolio.repository.ProyectosRepository;
import com.example.portfolio.repository.ServiciosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiciosService {
    @Autowired
    private ServiciosRepository serviciosRepository;

    public List<Servicios> getAll() {
        return serviciosRepository.findAll();
    }
}