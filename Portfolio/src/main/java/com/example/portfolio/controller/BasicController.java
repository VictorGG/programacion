package com.example.portfolio.controller;

import com.example.portfolio.model.*;
import com.example.portfolio.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class BasicController {
    @Autowired
    private HeaderService headerService;
    @Autowired
    private Sobre_miService sobre_miService;
    @Autowired
    private ServiciosService serviciosService;
    @Autowired
    private ProyectosService proyectosService;
    @Autowired
    private ContactoService contactoService;
    @Autowired
    private FooterService footerService;

    @GetMapping("/")
    String vertodo(Model model) {
        List<Header> headerList = headerService.getAll();
        model.addAttribute("header", headerList);
        List<Sobre_mi> sobre_miList =sobre_miService.getAll();
        model.addAttribute("sobremi", sobre_miList);
        List<Servicios> serviciosList =serviciosService.getAll();
        model.addAttribute("servicios", serviciosList);
        List<Proyectos> proyectosList =proyectosService.getAll();
        model.addAttribute("proyectos", proyectosList);
        List<Contacto> contactosList =contactoService.getAll();
        model.addAttribute("contactos", contactosList);
        List<Footer> footerList =footerService.getAll();
        model.addAttribute("footer", footerList);

        return "html";
    }

    @PostMapping("/")
    String contact(@RequestParam String nombre, @RequestParam String apellido, @RequestParam String correo, @RequestParam String mensaje, Model model) {
        Contacto c = new Contacto();
        c.setNombre(nombre);
        c.setApellido(apellido);
        c.setCorreo(correo);
        c.setMensaje(mensaje);

        contactoService.addContacto(c);
        model.addAttribute("mensaje", "Contacto realizado");

        List<Sobre_mi> sobre_miList =sobre_miService.getAll();
        model.addAttribute("sobre_mi", sobre_miList);
        List<Servicios> serviciosList =serviciosService.getAll();
        model.addAttribute("servicios", serviciosList);
        List<Proyectos> proyectosList =proyectosService.getAll();
        model.addAttribute("proyectos", proyectosList);

        return "html";
    }
}