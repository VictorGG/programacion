package com.example.portfolio.repository;

import com.example.portfolio.model.Header;
import com.example.portfolio.model.Proyectos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProyectosRepository extends JpaRepository<Proyectos, Long> {
}
