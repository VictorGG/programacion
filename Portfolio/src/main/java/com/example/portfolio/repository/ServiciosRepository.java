package com.example.portfolio.repository;

import com.example.portfolio.model.Header;
import com.example.portfolio.model.Servicios;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiciosRepository extends JpaRepository<Servicios, Long> {
}
