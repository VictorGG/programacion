create database Portfolio;
use Portfolio;

create table header(
id int primary key auto_increment,
nombre varchar(255),
fotos varchar(255)
);
insert into header values(1, "logo","./img/logo.png");

create table footer(
id int primary key auto_increment,
nombre varchar(255),
fotos varchar(255)
);
insert into footer values(1, "linkedin","./img/linkedin.png");
insert into footer values(2, "insta","./img/insta.png");

create table sobre_mi(
id int primary key auto_increment,
nombre varchar(255),
presentacion varchar(255)
);
insert into sobre_mi values(1,"VICTOR GARCIA, PROGRAMADOR","Hola a todos, soy Victor Garcia, Diseñador y desarrollador web con sede en Madrid, España
Con experiencia en diseño y desarrollo web y de apps.");

create table proyectos(
id int primary key auto_increment,
nombre varchar(255),
fotos varchar(255)
);
insert into proyectos values(1,"PROYECTO 1","./img/pro1.PNG");
insert into proyectos values(2,"PROYECTO 2","./img/pro2.PNG");
insert into proyectos values(3,"PROYECTO 3","./img/pro3.PNG");

create table servicios(
id int primary key auto_increment,
nombre varchar(255),
fotos varchar(255)
);
insert into servicios values(1,"html","./img/MicrosoftTeams-image (3).png");
insert into servicios values(2,"css","./img/css.png");
insert into servicios values(3,"java","./img/MicrosoftTeams-image (4).png");
insert into servicios values(4,"python","./img/python.png");

create table contacto(
id int primary key auto_increment,
nombre varchar (255),
apellido varchar (255),
correo varchar (255),
mensaje varchar (510)
);