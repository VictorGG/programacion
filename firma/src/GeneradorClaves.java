import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class GeneradorClaves {
    String algoritmo = "RSA";
    private PublicKey publicKey;
    private PrivateKey privateKey;



    public void generar(){

        File file = new File("clave privada");
        File file2 = new File("clave publica");
        try{
            FileOutputStream privada = new FileOutputStream(file);
            FileOutputStream publica = new FileOutputStream(file2);

            KeyPairGenerator keygen = KeyPairGenerator.getInstance(algoritmo);

            KeyPair keyPair = keygen.genKeyPair();

             publicKey = keyPair.getPublic();
             privateKey = keyPair.getPrivate();

            privada.write(privateKey.getEncoded());
            privada.close();

            publica.write(publicKey.getEncoded());
            publica.close();

            Cipher cipher = Cipher.getInstance(algoritmo);
            String mensaje = "Hola 2";

            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            byte[] mensajeCifrado = cipher.doFinal(mensaje.getBytes());

            System.out.println(new String(mensajeCifrado));

            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            byte[] mensjaeDescifrado = cipher.doFinal(mensajeCifrado);

            System.out.println(new String(mensjaeDescifrado));
            byte[] publickeyb = publicKey.getEncoded();
            byte[] privatekeyb = privateKey.getEncoded();

            X509EncodedKeySpec encode = new X509EncodedKeySpec(publickeyb);
            PKCS8EncodedKeySpec encodep = new PKCS8EncodedKeySpec(privatekeyb);

            KeyFactory kf = KeyFactory.getInstance(algoritmo);

            publicKey = kf.generatePublic(encode);
            privateKey = kf.generatePrivate(encodep);


        } catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | InvalidKeySpecException | IOException e) {
            throw new RuntimeException(e);
        }

    }
}
