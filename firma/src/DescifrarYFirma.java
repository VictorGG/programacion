import javax.crypto.Cipher;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class DescifrarYFirma {
    private String algoritmo ="RSA";
    public byte[] publicKey;
    private byte[] mensajeCifrado;

    public DescifrarYFirma(byte[] publicKey, byte[] mensajeCifrado) {
        this.publicKey = publicKey;
        this.mensajeCifrado = mensajeCifrado;
    }

    public void descifrar() {
        try {
            X509EncodedKeySpec encodep = new X509EncodedKeySpec(publicKey);
            KeyFactory kf = KeyFactory.getInstance(algoritmo);
            PublicKey publicKey = kf.generatePublic(encodep);
            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            byte[] mensjaeDescifrado = cipher.doFinal(mensajeCifrado);
            System.out.println(new String(mensjaeDescifrado));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
