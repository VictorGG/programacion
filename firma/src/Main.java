import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PublicKey;
import java.util.Scanner;

public class Main {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int respuesta;
        do {
            menu();
            respuesta=sc.nextInt();
        }while(respuesta<1 || respuesta>4);

        switch(respuesta){
            case 1: generarClaves();
            break;
            case 2: cifrarFirmar();
            break;
            case 3: descifraryFirma();
            break;
            case 4:
            break;
        }


    }

    public static void menu() {
        System.out.println();
        System.out.println("Opciones: " +
                        "1. Generar claves" +
                        "2. Cifrar y firmar" +
                        "3. Descifrar" +
                        "4. Salir");
    }

    public static void generarClaves() {
        GeneradorClaves generadorClaves = new GeneradorClaves();
        generadorClaves.generar();
    }

    public static void cifrarFirmar() {
        System.out.print("Dime el nombre de la clave privada: ");
        byte[] clavePrivada = cat(sc.nextLine());
        System.out.print("Mensaje que quieres cifrar: ");
        String mensaje = sc.nextLine();
        System.out.println("Nombre con el quieres guardar el mensaje cifrado: ");
        String mensajeCifrado = sc.nextLine();
        System.out.println("Nombre con el quieres guardar la firma");
        String firma = sc.nextLine();

        CifradoYFirma cifradoYFirma = new CifradoYFirma(clavePrivada, mensaje, firma);
        cifradoYFirma.cifrarMensaje();
    }

    public static void descifraryFirma() {
        System.out.print("Dime el nombre de la clave publica: ");
        byte[] clavePublica = cat(sc.nextLine());
        System.out.print("Dime el nombre del mensaje cifrado: ");
        byte[] mensajeCifrado = cat(sc.nextLine());
        System.out.print("Dime el nombre de la firma: ");
        String firma = sc.nextLine();

        DescifrarYFirma descifrarYFirma = new DescifrarYFirma(clavePublica, mensajeCifrado);
        descifrarYFirma.descifrar();
    }

    public static void mkfile(byte[] clave, String nombreFile) {
        Path ruta = Paths.get(System.getProperty("user.dir"));
        Path ruta_fichero = ruta.resolve(nombreFile);
        File file = new File(ruta_fichero.toString());
        //Si NO existe
        if (!file.exists()) {
            try {
                if (file.createNewFile()) {
                    System.out.println(nombreFile + " creado");
                }
            } catch (IOException e) {
                System.out.println("No se pudo crear el fichero");
            }
            try {
                FileOutputStream fos = new FileOutputStream(nombreFile);
                fos.write(clave);
                fos.close();

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else System.out.println("El fichero " + nombreFile + " ya existe.");
    }

    public static byte[] cat(String nombreFile) {
        byte[] clave = new byte[0];
        Path ruta = Paths.get(System.getProperty("user.dir"));
        Path ruta_fichero = ruta.resolve(nombreFile);
        File file = new File(ruta_fichero.toString());
        if (file.exists() && file.isFile()) {
            try {
                clave = new FileInputStream(nombreFile).readAllBytes();

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            System.out.println("El archivo " + nombreFile + " no se ha sido encontrado");
        }
        return clave;
    }
}
