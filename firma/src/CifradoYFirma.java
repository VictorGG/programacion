import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.Scanner;


public class CifradoYFirma {
    public String mensaje;
    private String algoritmo="RSA";
    private byte[] privateKey;
    private String firma;


    public CifradoYFirma(byte[] privateKey, String mensaje) {
        this.privateKey = privateKey;
        this.mensaje = mensaje;
    }

    public void cifrarMensaje() {
        try {
            byte[] privateKeyb = privateKey;
            PKCS8EncodedKeySpec encodep = new PKCS8EncodedKeySpec(privateKeyb);
            KeyFactory kf = KeyFactory.getInstance(algoritmo);
            PrivateKey privateKey = kf.generatePrivate(encodep);


            Signature privateSignature = Signature.getInstance("SHA256withRSA");
            privateSignature.initSign(privateKey);

            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            byte[] mensajeCifrado = cipher.doFinal(mensaje.getBytes(StandardCharsets.UTF_8));
            MainEj.mkfile(mensajeCifrado, "mensaje_cifrado");
            privateSignature.update(mensajeCifrado);
            byte[] firma = privateSignature.sign();
            MainEj.mkfile(firma, "firmado");
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | BadPaddingException | InvalidKeyException |
                 IllegalBlockSizeException | NoSuchPaddingException | SignatureException e) {
            throw new RuntimeException(e);
        }


    }
}
