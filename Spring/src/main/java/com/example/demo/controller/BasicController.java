package com.example.demo.controller;

import com.example.demo.model.Movie;
import com.example.demo.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class BasicController {
    @Autowired
    private MovieService movieService;

    @RequestMapping("/")
    String hola(){
        return "index";
    }

    @RequestMapping("/add")
    String add(){
        return "addMovie";
    }

    @PostMapping("/add")
    String prueba(@RequestParam String movie_name, @RequestParam String url, Model model){
        Movie m=new Movie();
        m.setUrl(url);
        m.setMovie_name(movie_name);

        movieService.addMovie(m);

        model.addAttribute("message","La pelicula "+movie_name+" ha sido añadida");

        return "index";
    }

    @RequestMapping("/edit")
    String update(){
        return "editMovie";
    }
    @PutMapping("/edit")
    String edit(@RequestParam String movie_name, @RequestParam String url, @RequestParam Long movie_id, Model model) {
        Movie m = new Movie();
        m.setUrl(url);
        m.setMovie_name(movie_name);
        movieService.updateMovie(movie_id, m);

        model.addAttribute("message", "La pelicula '" + movie_name + "' ha sido editada.");

        return "index";
    }


    @RequestMapping("/del")
    String delete(){
        return "deleteMovie";
    }
    @DeleteMapping("/del")
    String prueba2(@RequestParam Long id, Model model) {
        movieService.deleteMovie(id);

        model.addAttribute("message", "La película '" + id + "' ha sido borrada.");

        return "index";
    }
}
